# Kevin's CRM and Support Site

## Instalation 

* Clone this repo
* Run `composer install`
* Run `npm install`
* Import the `install.sql` script in the `work` folder
* Create a new file: `work/config.json`
* Copy and paste the following inside `work/config.json` and make sure to change what is necessary
```json
{
    "debug_app" : true|false,
    "mainkey" : "",
    "mainsecret" : "",
    "database" : {
        "schema" : "",
        "server" : "",
        "username" : "",
        "password" : ""
    },
    "smtp" : {
        "debug" : true|false,
        "server" : "",
        "port" : "",
        "username" : "",
        "password" : "",
        "security" : "",
        "fromname" : "",
        "fromemail" : "",
        "forcehtml" : true|false,
        "signature" : ""
    },
    "recaptcha" : {
        "sitekey" : "",
        "secretkey" : "",
        "expectedhostname" : ""
    }
}
```
* Once you have everything you need to go live, run `gulp`
