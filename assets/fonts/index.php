<?php
/**
 * index.php
 * 
 * No direct access allowed!
 * 
 * @since 7.4
 * @author Kevin Pirnie <me@kpirnie.com>
 * @package Kevin's CRM and Support
 * 
 */

// define the primary app path if not already defined
defined( 'CRM_PATH' ) || die( 'Direct Access is not allowed!' );
