// require gulp
import gulp from "gulp";

// require delete
import deleter from "del";

// require sass plugin
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass( dartSass );

// require js concat and minify plugins
import uglify from "gulp-uglify";
import gconcat from "gulp-concat";
import grename from "gulp-rename";

// require css minifier
import cssnano from "gulp-cssnano";

// require image optimizer
import imagemin from "gulp-imagemin";

// svgs
import svgo from "gulp-svgo";

// fs
import fs from "fs";

// read in our package json file
const pkg = JSON.parse( fs.readFileSync( './package.json' ) )

// our working glob paths
const globs = {
    src: {
        js: [ 
            `assets/js/*.js`,
            `!assets/js/custom.js`
        ],
        scss: [ 
            `assets/js/*.scss` 
        ],
        css: [ 
            `assets/css/*.css`,
            `!assets/css/custom.css`
        ],
        fonts: [ 
            `assets/fonts/**/*` 
        ],
        img: [ 
            `assets/images/*.+(png|jpg|jpeg|gif)` 
        ],
        svgs: [ 
            `assets/images/*.+(svg|svgz)` 
        ]
    },
};

/** Setup our tasks to run */

// cleanup concat files
gulp.task( 'cleanupconcat', function( ) {
    console.log( '# Cleaning Up Concatenated Files' );
    return deleter( [`assets/css/concat.css`, `assets/css/temp.css`, `assets/js/concat.js`] );
} );

// sass
gulp.task( 'sass', function( ) {
    console.log( '# Working on SASS' );
    return gulp.src( globs.src.scss )
        .pipe( sass.sync( ).on( 'error', sass.logError ) )
        .pipe( gulp.dest( `assets/css/` ) );
} );

// css
gulp.task( 'stylesheets', function( ) {
    console.log( '# Working on Stylesheets' );
    return gulp.src( globs.src.css )
        .pipe( gconcat( 'concat.css' ) )
        .pipe( gulp.dest( `assets/css/` ) )
        .pipe( grename( 'style.min.css' ) )
        .pipe( cssnano( ) )
        .pipe( gulp.dest( `assets/css/` ) );
} );

// javascript
gulp.task( 'javascripts', function( ) {
    console.log( '# Working on JS' );
    return gulp.src( globs.src.js )
        .pipe( gconcat( 'concat.js' ) )
        .pipe( gulp.dest( `assets/js/` ) )
        .pipe( grename( 'script.min.js' ) )
        .pipe( uglify( ) )
        .pipe( gulp.dest( `assets/js/` ) );
} );

// svgs
gulp.task( 'svgs', function( ) {
    console.log( '# Working on SVGs' );
    return gulp.src( globs.src.svgs )
        .pipe( svgo( ) )
        .pipe( gulp.dest( `assets/images/` ) );
} );

// images
gulp.task( 'images', function( ) {
    console.log( '# Working on Images' );
    return gulp.src( globs.src.img )
        .pipe( imagemin( ) )
        .pipe( gulp.dest( `assets/images/` ) );
} );

// setup our default task to run our build sequencing
gulp.task( 'default', gulp.series(
    'sass',
    'stylesheets',
    'javascripts',
    'cleanupconcat',
    'svgs',
    'images',
) );
