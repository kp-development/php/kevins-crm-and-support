<?php
/**
 * index.php
 * 
 * This is our primary starting point for this app
 * here, we are going to set the primary paths to be
 * utilize, include the default configuration
 * and pull in everything necessary to run the application
 * 
 * @since 7.4
 * @author Kevin Pirnie <me@kpirnie.com>
 * @package Kevin's CRM and Support
 * 
 */

// define the primary app path if not already defined
defined( 'CRM_PATH' ) || define( 'CRM_PATH', dirname( __FILE__ ) . '/' );
